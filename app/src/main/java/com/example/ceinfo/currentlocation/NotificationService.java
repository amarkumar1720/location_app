package com.example.ceinfo.currentlocation;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by CEINFO on 27-05-2016.
 */
public class NotificationService extends Service
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    NotificationCompat.Builder mBuilder;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    boolean run = true;
    private DBHelper mydb ;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * initialised APIClient, LocationRequest
     * regularly check if settings available
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = new LocationRequest()
                .setInterval(10000)
                .setFastestInterval(10000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mydb = new DBHelper(this);
        (new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 0;
                while(true){
                    try {
                        if (run){
                            count = 0;

                            if (ContextCompat.checkSelfPermission(NotificationService.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                                Intent intent = new Intent(Constants.BROADCAST_ACTION);
                                intent.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                                sendBroadcast(intent);
                            }

                            if (Settings.Secure.getInt(NotificationService.this.getContentResolver(), Settings.Secure.LOCATION_MODE)!=3){
                                Intent other = new Intent(Constants.BROADCAST_ACTION);
                                other.putExtra(Constants.MESSAGE, Constants.LOCATION_REQUEST);
                                sendBroadcast(other);
                                run = false;
                            }
                        }else{
                            if (count < 50){
                                count++;
                            }
                            if (count == 1){
                                //TODO
//                                mBuilder.setContentText("Location settings changed");
//                                startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
                            }
                        }
                    } catch (Settings.SettingNotFoundException e) {
                        Intent other = new Intent(Constants.BROADCAST_ACTION);
                        other.putExtra(Constants.MESSAGE, Constants.EXIT_REQUEST);
                        sendBroadcast(other);
                        exit();
                    }

                    try {
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        //Thread never interrupted
                        e.printStackTrace();
                    }
                }
            }
        })).start();

    }

    /**
     * set the shared preferences boolean based on whether service is starting or not
     *
     * if start called-->
     * connect with API client
     * go to main activity on clicking notification
     * start a foreground service
     *
     * if stop called -->
     * exit
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        run = true;

        if (intent == null||intent.getAction().equals(Constants.START_SERVICE)) {

            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
            editor.putBoolean(Constants.SERVICE_STATUS, true);
            editor.commit();

            if (mGoogleApiClient.isConnected()){
//                Log.d(Constants.TAG, "Restarting location updates...");
                startLocationUpdates();
            }else{
                mGoogleApiClient.connect();
            }
//            Log.d(Constants.TAG, "Connecting....");

            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction(Constants.MAIN_ACTION);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);

            mBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Current Location")
                    .setTicker("Current Location")
                    .setContentText("your location")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true);

            startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        } else if (intent.getAction().equals(Constants.STOP_SERVICE)) {

            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
            editor.putBoolean(Constants.SERVICE_STATUS, false);
            editor.commit();

            exit();
        }

        return START_STICKY;
    }

    /**
     * stop the service
     */
    public void exit(){
        mGoogleApiClient.disconnect();
        stopForeground(true);
        stopSelf();
    }

    /**
     * request location updates if permission present
     */
    public void startLocationUpdates(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //TODO nothing
        }else{
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this
            );
//            Log.d(Constants.TAG, "Requesting location updates...");
        }
    }

    /**
     * callback method after connecting
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
//        Log.d(Constants.TAG, "Starting location updates...");
    }


    @Override
    public void onConnectionSuspended(int i) {
//        Log.d(Constants.TAG, "Connection suspended");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mBuilder.setContentTitle("Connection suspended");
        mBuilder.setContentText("Reconnecting...");
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            //Not reached
            e.printStackTrace();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(Constants.TAG, "Location Changed...");
        mydb.addlocation(location);
        sendtodb();
        mBuilder.setContentText("Co-ordinates:"+location.getLatitude()+","+location.getLongitude());
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void sendtodb()
    {
        boolean connected = isNetworkAvailable();

        if(connected)
        {
            ArrayList<String[]> locations = mydb.getAlllocations();
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            for(int i =0; i<locations.size(); i++)
            {  final String[] details  = locations.get(i);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.add_location_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("longitude",details[0]);
                        params.put("latitude", details[1]);
                        params.put("altitude", details[2]);
                        params.put("accuracy", details[3]);
                        params.put("bearing", details[4]);
                        params.put("speed", details[5]);
                        params.put("provider", details[6]);
                        params.put("time", details[7]);
                        return params;
                    }


                };

                requestQueue.add(stringRequest);
            }

        }
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        Log.d(Constants.TAG, "Connection failed");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mBuilder.setContentTitle("Connection failed");
        mBuilder.setContentText("Reconnecting...");
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            //Not reached
            e.printStackTrace();
        }
        mGoogleApiClient.connect();
    }
}
