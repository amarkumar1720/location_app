package com.example.ceinfo.currentlocation;

import android.content.Context;

/**
 * Created by CEINFO on 27-05-2016.
 */
public class Constants {

    public static final int mId = 0;
    public static final String TAG = "CUSTOM_TAG";
    public static final int REQUEST_ACCESS_FINE_LOCATION = 0;

    public static final String BROADCAST_ACTION = "BROADCAST_ACTION";

    public static final String SERVICE_STATUS = "SERVICE_STATUS";
    public static final String PREFS = "PREFS";

    public static final String START_SERVICE = "START_SERVICE";
    public static final String STOP_SERVICE = "STOP_SERVICE";
    public static final String MAIN_ACTION = "MAIN_ACTION";

    public static final int FOREGROUND_SERVICE = 101;
    public static final String MESSAGE = "MESSAGE";
    public static final String add_location_url = "";
    public static final int PERMISSION_REQUEST = 0;
    public static final int LOCATION_REQUEST = 1;
    public static final int EXIT_REQUEST = 2;
}
