package com.example.ceinfo.currentlocation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 *
 */
public class MainActivity extends AppCompatActivity {

    boolean service_running;
    Button myButton;

    /**
     * customize a button depending upon whether service is running or not
     * created a receiver that receives broadcasts
     * initialised service running
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myButton = (Button) findViewById(R.id.myButton);

        //register with the given filter
        MyReceiver myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_ACTION);
        registerReceiver(myReceiver, intentFilter);

        service_running = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).getBoolean(Constants.SERVICE_STATUS, false);

        customizeButton();
    }

    /**
     * customizes button based on whether service running or not
     */
    public void customizeButton(){

        if (service_running){
            myButton.setText("Stop Service");
            myButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            stopNotificationService();
                        }
                    }
            );
        }else{
            myButton.setText("Start Service");
            myButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startNotificationService();
                        }
                    }
            );
        }
    }

    /**
     * starts notification service
     */
    public void startNotificationService(){
        Intent startIntent = new Intent(MainActivity.this, NotificationService.class);
        startIntent.setAction(Constants.START_SERVICE);
        startService(startIntent);
//        Log.d(Constants.TAG, "Starting Service...");
        service_running = true;
        customizeButton();
    }

    /**
     * stops notification service
     */
    public void stopNotificationService(){
        Intent stopIntent = new Intent(MainActivity.this, NotificationService.class);
        stopIntent.setAction(Constants.STOP_SERVICE);
        startService(stopIntent);
//        Log.d(Constants.TAG, "Stopping Service...");
        service_running = false;
        customizeButton();
    }

    /**
     * broadcast receiver
     */
    class MyReceiver extends BroadcastReceiver{


        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(Constants.TAG, "Permissions requested......");
            int request = intent.getIntExtra(Constants.MESSAGE, -1);
            if (request == Constants.PERMISSION_REQUEST){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        Constants.REQUEST_ACCESS_FINE_LOCATION);
            }else if (request == Constants.LOCATION_REQUEST){
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Settings required")
                        .setMessage("Turn on high accuracy mode")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        stopNotificationService();
                        exit();
                    }
                });
                builder.create().show();
            }else if (request == Constants.EXIT_REQUEST){
                exit();
            }
        }
    }

    public void exit(){
        MainActivity.this.finish();
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Log.d(Constants.TAG, "Permissions result....");
        switch (requestCode){
            case Constants.REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    startNotificationService();
                }else{
                    stopNotificationService();
                    exit();
                }
                break;
            default:stopNotificationService();
                break;
        }
    }
}
