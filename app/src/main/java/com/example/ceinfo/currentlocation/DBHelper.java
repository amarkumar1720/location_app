package com.example.ceinfo.currentlocation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

import java.util.ArrayList;

/**
 * Created by ce on 30-May-16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table locations " +
                        "(id INTEGER primary key AUTO_INCREMENT, longitude DOUBLE,latitude DOUBLE,altitude DOUBLE,accuracy FLOAT,bearing FLOAT," +
                        "speed FLOAT,status INTEGER,provider VARCHAR(8),time BIGINT"

        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS locations");
        onCreate(db);
    }
    public void addlocation(Location location)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("longitude", location.getLongitude());
        contentValues.put("latitude", location.getLatitude());
        contentValues.put("altitude", location.getAltitude());
        contentValues.put("accuracy", location.getAccuracy());
        contentValues.put("bearing", location.getBearing());
        contentValues.put("speed", location.getSpeed());
        contentValues.put("status", 0);
        contentValues.put("provider",location.getProvider());
        contentValues.put("time", location.getTime());
        db.insert("locations", null, contentValues);
    }

    public void updatestatus(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update locations set status = 1 where id ="+id+"");
    }

    public ArrayList<String[]> getAlllocations()
    {
        ArrayList<String[]> array_list = new ArrayList<String[]>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from location where status="+0+"", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            String[] data = new String[8];
            data[0]= res.getString(res.getColumnIndex("longitude"));
            data[1]= res.getString(res.getColumnIndex("latitude"));
            data[2]= res.getString(res.getColumnIndex("altitude"));
            data[3]= res.getString(res.getColumnIndex("accuracy"));
            data[4]= res.getString(res.getColumnIndex("bearing"));
            data[5]= res.getString(res.getColumnIndex("speed"));
            data[6]= res.getString(res.getColumnIndex("provider"));
            data[7]= res.getString(res.getColumnIndex("time"));
            array_list.add(data);
            updatestatus(res.getColumnIndex("id"));
            res.moveToNext();
        }
        return array_list;
    }
    public Integer clear(Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

}
